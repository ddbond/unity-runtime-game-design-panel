﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Gameplay.GdPanel.Attributes;
using Gameplay.GdPanel.UiGdPanel.Categories;
using Gameplay.GdPanel.UiGdPanel.Elements;
using Sirenix.Utilities;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Object = UnityEngine.Object;

namespace Gameplay.GdPanel.Static
{
    internal class GameDesignPanel : MonoBehaviour
    {
        internal const string ROOT_CATEGORY_PATH = "Game Design Panel";

        internal static string[] GetLevelsPath(string path)
        {
            return path.Split('/');
        }

        [SerializeField] private GameDesignPanelSettings _settings;
        [SerializeField] private GameObject _panel;
        [SerializeField] private Button _backButton;
        [SerializeField] private Button _closeButton;
        [SerializeField] private TextMeshProUGUI _currentCatalogLabel;
        [SerializeField] private Transform _elementsRoot;
        [SerializeField] private Transform _categoriesRoot;
        [SerializeField] private bool autoSetCanvasScalerResolution = true;

        #region Core

        private class AttributeInfo
        {
            public readonly string Name;
            public readonly string Path;

            public AttributeInfo(string name, string path)
            {
                Name = name;
                Path = path;
            }
        }

        private static GameDesignPanel _instance;

        private void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                DestroyImmediate(gameObject);
            }
        }

        private void Start()
        {
            _backButton.onClick.AddListener(ClickBackButton);

            if (autoSetCanvasScalerResolution)
                GetComponent<CanvasScaler>().referenceResolution =
                    new Vector2(Screen.currentResolution.width, Screen.currentResolution.height);
        }

        private void Initialize()
        {
            //TODO: добавить поддержку ScriptableObject
            _pathToElement = new Dictionary<string, List<GameObject>>();
            _pathToCategory = new Dictionary<string, GameObject>();
            _groupsList = new List<GameObject>();

            foreach (Transform child in _categoriesRoot)
            {
                Destroy(child.gameObject);
            }

            foreach (Transform child in _elementsRoot)
            {
                Destroy(child.gameObject);
            }

            FindExposeAttribute();

            ShowInformationOnPath(ROOT_CATEGORY_PATH);
        }

        private void FindExposeAttribute()
        {
            var allMonos = FindObjectsOfType<MonoBehaviour>().Select(e => e as Object).ToList();

            var allMonosAndSO = new List<Object>(allMonos);
            //Чтобы найти используемые сейчас скриптаблы ищем среди всех полей всех присутствующих на данный момент монобехов те,
            //тип которых наследован от ScriptableObject. Берем их значения (ссылки на конкретные скриптаблы) и добавляем в общий список к самим монобехам.
            foreach (var ScriptableObjectsInMono in allMonos.Select(mono => mono.GetType()
                .GetFields(BindingFlags.NonPublic |
                           BindingFlags.Public |
                           BindingFlags.Instance)
                .Where(e => e.FieldType.IsSubclassOf(typeof(ScriptableObject)))
                .Select(e => e.GetValue(mono) as Object)))
            {
                allMonosAndSO.AddRange(ScriptableObjectsInMono);
            }

            foreach (var obj in allMonosAndSO.Distinct())
            {
                if (obj == null) continue;
                var objType = obj.GetType();

                List<GameObject> createdElements = FindFieldsWithAttribute(objType, obj);
              
                var methodContexts = GetNameMethodWithAttribute(objType);

                if (methodContexts.Count == 0) continue;
                foreach (var methodContext in methodContexts)
                {
                    var methodView = CreateMethodElement(methodContext.Name, obj, methodContext.Name);
                    createdElements.Add(methodView.gameObject);
                    AddGdElement(methodContext.Path, methodView.gameObject);
                    TryCreateCategory(methodContext.Path);
                }
                
                if (createdElements.Count > 0)
                {
                    var group = Instantiate(_settings.GetGroupPrefab, _elementsRoot, false);
                    group.Init($"{obj.name} -> {objType.ToString().Split('.').Last()}");
                    _groupsList.Add(group.gameObject);
                    foreach (var createdElement in createdElements)
                    {
                        createdElement.transform.SetParent(group.transform);
                    }
                }
            }
        }

        /// <summary>
        /// Найти атрибут на методе
        /// </summary>
        /// <param name="monoType"></param>
        private static List<AttributeInfo> GetNameMethodWithAttribute(Type monoType)
        {
            List<AttributeInfo> attributeInfos = new List<AttributeInfo>();
            foreach (var method in monoType.GetMethods(
                BindingFlags.NonPublic | BindingFlags.Static |
                BindingFlags.Public |
                BindingFlags.Instance))
            {
                var attributes = method.GetCustomAttributes(typeof(GDPanelExposeAttribute), true);
                if (attributes.Length <= 0) continue;
                var attributePath = (attributes[0] as GDPanelExposeAttribute)?.Path;
                attributeInfos.Add(new AttributeInfo(method.Name, attributePath));
            }

            return attributeInfos;
        }

        /// <summary>
        /// Найти атрибут в поле
        /// </summary>
        /// <param name="objType"></param>
        /// <param name="obj"></param>
        /// <param name="group"></param>
        /// <param name="attribute"></param>
        private List<GameObject> FindFieldsWithAttribute(Type objType, Object obj)
        {
            List<GameObject> outputList = new List<GameObject>();
            var wasFound = false;
            foreach (var field in objType.GetFields(
                BindingFlags.NonPublic |
                BindingFlags.Public | BindingFlags.Static |
                BindingFlags.Instance))
            {
                var attributes = field.GetCustomAttributes(typeof(GDPanelExposeAttribute), true);

                void TryAddElementAndCreateCategory(GDPanelElement gdPanelElementInt)
                {
                    var path = (attributes[0] as GDPanelExposeAttribute)?.Path;
                    AddGdElement(path, gdPanelElementInt.gameObject);
                    TryCreateCategory(path);
                }

                if (attributes.Length <= 0) continue;
                var prefab = _settings.GetElementPrefab(field.FieldType);

                if (prefab == null)
                {
                    Debug.LogWarning($"Не удалось добавить поле {field.Name} в ГД панель!");
                    continue;
                }

                var valueView = CreateFieldElement(prefab, field.Name, field.GetValue(obj), obj, field.Name);
                outputList.Add(valueView.gameObject);
                TryAddElementAndCreateCategory(valueView);
                wasFound = true;
            }

            return outputList;
        }

        /// <summary>
        /// Создать категорию (меню на гд панели), если это необходимо
        /// </summary>
        /// <param name="attributePath"></param>
        private void TryCreateCategory(string attributePath)
        {
            if (!HasCategory(attributePath))
            {
                var nameCategory = GetLevelsPath(attributePath)
                    .Reverse()
                    .First();
                var categoryView = CreateCategory(nameCategory, attributePath);
                AddGdCategory(attributePath, categoryView.gameObject);
                categoryView.OnSelectCategory += ShowInformationOnPath;
            }

            CheckPreviousCategoryPath(attributePath);
        }

        /// <summary>
        /// Проверить, существует ли предыдущая категория
        /// </summary>
        /// <param name="path"></param>
        private void CheckPreviousCategoryPath(string path)
        {
            var allPath = GetLevelsPath(path);
            if (allPath.Length <= 1) return;
            var resultPath = "";
            for (var i = 0; i < allPath.Length - 1; i++)
            {
                resultPath += allPath[i] + "/";
            }

            TryCreateCategory(resultPath.Substring(0, resultPath.Length - 1));
        }


        #region CREATE VIEWS

        private GDPanelElementMethod CreateMethodElement(string description, Object obj, string source)
        {
            var instance = Instantiate(_settings.GetMethodPrefab);
            instance.Initialization(description, obj, source);
            return instance;
        }

        private BaseGDPanelCategory CreateCategory(string description, string path)
        {
            var instance = Instantiate(_settings.GetCategoryPrefab, _categoriesRoot, false);
            instance.Initialization(description, path);
            return instance;
        }

        private GDPanelElement CreateFieldElement(GDPanelElementField prefab, string description, object value,
            Object originObj, string nameField)
        {
            var instance = Instantiate(prefab);
            instance.InitAndSetValue(description, value, originObj, nameField);
            return instance;
        }

        #endregion

        #endregion

        #region UI Controller

        private Dictionary<string, List<GameObject>> _pathToElement
            = new Dictionary<string, List<GameObject>>();

        private Dictionary<string, GameObject> _pathToCategory
            = new Dictionary<string, GameObject>();

        private List<GameObject> _groupsList = new List<GameObject>();

        private string _backPath;

        public void ShowPanel()
        {
            Initialize();
            _panel.SetActive(true);
        }

        private void ClickBackButton()
        {
            ShowInformationOnPath(_backPath);
        }

        private void AddGdElement(string path, GameObject elementObject)
        {
            if (!_pathToElement.ContainsKey(path))
            {
                _pathToElement.Add(path, new List<GameObject>());
            }

            _pathToElement[path].Add(elementObject);
            elementObject.SetActive(false);
        }

        private void AddGdCategory(string path, GameObject categoryObject)
        {
            if (_pathToCategory.ContainsKey(path)) return;
            _pathToCategory.Add(path, categoryObject);
            categoryObject.SetActive(false);
        }

        private bool HasCategory(string path)
        {
            return _pathToCategory.ContainsKey(path);
        }

        private void ShowInformationOnPath(string path)
        {
            HideAllElements();
            HideAllCategories();
            ShowElements(path);
            ShowCategories(path);
            HideEmptyGroups();
            SetBackButton(path);
            SetCurrentCatalogName(path);
            RefreshLayouts();
        }

        private void HideEmptyGroups()
        {
            foreach (var group in _groupsList)
            {
                //Скрываем объекты групп, в которых нет ни одного активного элемента
                @group.SetActive(@group.GetComponentsInChildren<GDPanelElement>()
                    .Where(e => e.gameObject.activeSelf).ToList().Count != 0);
            }
        }

        private void HideAllElements()
        {
            foreach (var element in _pathToElement.Keys.SelectMany(pathElement => _pathToElement[pathElement]))
            {
                element.SetActive(false);
            }
        }

        private void HideAllCategories()
        {
            foreach (var pathCategory in _pathToCategory.Keys)
            {
                _pathToCategory[pathCategory].SetActive(false);
            }
        }

        private void ShowElements(string path)
        {
            if (!_pathToElement.ContainsKey(path)) return;
            foreach (var element in _pathToElement[path])
            {
                element.SetActive(true);
            }
        }

        private void ShowCategories(string path)
        {
            var currentPathLevel = GetLevelsPath(path);
            if (!_pathToCategory.ContainsKey(path)) return;
            foreach (var categoryPath in _pathToCategory.Keys)
            {
                var categoryPathLevel = GetLevelsPath(categoryPath);
                if (categoryPathLevel.Length != currentPathLevel.Length + 1) continue;
                var correctPath = !currentPathLevel.Where((t, i) => t != categoryPathLevel[i]).Any();

                if (correctPath)
                {
                    _pathToCategory[categoryPath].SetActive(true);
                }
            }
        }

        private void SetBackButton(string path)
        {
            var currentPathLevel = GetLevelsPath(path);
            if (currentPathLevel.Length == 1)
            {
                _backButton.gameObject.SetActive(false);
                _closeButton.gameObject.SetActive(true);
                return;
            }

            _backButton.gameObject.SetActive(true);
            _closeButton.gameObject.SetActive(false);
            var resultPath = "";
            for (var i = 0; i < currentPathLevel.Length - 1; i++)
            {
                resultPath += currentPathLevel[i] + "/";
            }

            _backPath = resultPath.Substring(0, resultPath.Length - 1);
        }

        private void SetCurrentCatalogName(string path)
        {
            var currentPathLevel = GetLevelsPath(path);
            _currentCatalogLabel.text = currentPathLevel.Last();
        }

        private void RefreshLayouts()
        {
            LayoutRebuilder.ForceRebuildLayoutImmediate(_elementsRoot.transform as RectTransform);
            LayoutRebuilder.ForceRebuildLayoutImmediate(_categoriesRoot.transform as RectTransform);
        }

        #endregion
    }
}