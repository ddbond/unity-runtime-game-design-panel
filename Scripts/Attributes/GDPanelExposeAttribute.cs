﻿using System;
using Gameplay.GdPanel.Static;

namespace Gameplay.GdPanel.Attributes
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Field)]
    public class GDPanelExposeAttribute : Attribute
    {
        internal string Path = GameDesignPanel.ROOT_CATEGORY_PATH;
        public GDPanelExposeAttribute()
        {
        }
        
        public GDPanelExposeAttribute(string path)
        {
            Path += "/" + path;
        }
        
    }
}
