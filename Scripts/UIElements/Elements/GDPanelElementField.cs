﻿using System.Reflection;
using UnityEngine;

namespace Gameplay.GdPanel.UiGdPanel.Elements
{
    internal abstract class GDPanelElementField : GDPanelElement
    {
        internal void InitAndSetValue(string description, object value, Object mono, string fieldName)
        {
            Initialization(description, mono, fieldName);
            SetValue(value);
        }
        
        protected virtual void SetValue(object value)
        {
            var monoType = _originObj.GetType();
            var targetField = monoType.GetField(_attributeSource, 
                BindingFlags.NonPublic | 
                BindingFlags.Public | BindingFlags.Static |
                BindingFlags.Instance);
            targetField.SetValue(_originObj, value);
        }
    }
}