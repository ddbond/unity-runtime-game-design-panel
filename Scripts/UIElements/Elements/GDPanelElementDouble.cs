﻿namespace Gameplay.GdPanel.UiGdPanel.Elements
{
    internal class GDPanelElementDouble : GDPanelElementInputField
    {
        protected override void SaveValue(string text)
        {
            var value = double.Parse(_inputField.text);
            SetValue(value);
            ShowEditPanel(false);
        }

        protected override void SetValue(object value)
        {
            if (value is double convertedValue)
                SetValue(convertedValue);
        }

        private void SetValue(double value)
        {
            base.SetValue(value);
        }
    }
}
