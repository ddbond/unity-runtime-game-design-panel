using TMPro;
using UnityEngine;

namespace Gameplay.GdPanel.UiGdPanel.Elements
{
    internal class GDPanelGroup : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _instanceNameText;

        internal void Init(string instanceNameText)
        {
            _instanceNameText.text = instanceNameText;
        }
    }
}