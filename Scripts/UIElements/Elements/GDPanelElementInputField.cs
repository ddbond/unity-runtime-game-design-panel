﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Gameplay.GdPanel.UiGdPanel.Elements
{
    internal class GDPanelElementInputField : GDPanelElementField
    {
        [SerializeField] private Button _editButton;
        [SerializeField] protected TMP_InputField _inputField;
        [SerializeField] protected TextMeshProUGUI _valueTmp;

        private void OnEnable()
        {
            _editButton.onClick.AddListener(SwitchEditing);
            _inputField.onEndEdit.AddListener(SaveValue);
        }

        private void OnDisable()
        {
            _editButton.onClick.RemoveListener(SwitchEditing);
            _inputField.onEndEdit.RemoveListener(SaveValue);
        }

        protected virtual void SaveValue(string text)
        {
            SetValue(text);
            ShowEditPanel(false);
        }

        protected override void SetValue(object value)
        {
            base.SetValue(value);
            _valueTmp.text = _inputField.text = value.ToString();
        }

        private void SwitchEditing()
        {
            ShowEditPanel(!_isEditingNow);
        }

        private bool _isEditingNow;

        protected void ShowEditPanel(bool state)
        {
            _isEditingNow = state;
            _valueTmp.gameObject.SetActive(!state);
            _inputField.gameObject.SetActive(state);
            if (state)
                _inputField.ActivateInputField();
            LayoutRebuilder.ForceRebuildLayoutImmediate(transform as RectTransform);
            LayoutRebuilder.ForceRebuildLayoutImmediate(transform.parent as RectTransform);
        }
    }
}