﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Gameplay.GdPanel.UiGdPanel.Elements
{
    internal class GDPanelElementMethod : GDPanelElement
    {
        [SerializeField] private Button _button;

        internal override void Initialization(string description, Object obj, string source)
        {
            base.Initialization(description,obj,source);
            AfterInitialization();
        }
        
        private void AfterInitialization()
        {
            var info = UnityEventBase.GetValidMethodInfo(_originObj, _attributeSource, new System.Type[0]);
            
            //возможно обработать параметры
            UnityAction execute = () => info.Invoke(_originObj, info.GetParameters());
            _button.onClick.AddListener(execute);    
        }
    }
}
