﻿namespace Gameplay.GdPanel.UiGdPanel.Elements
{
    internal class GDPanelElementInt : GDPanelElementInputField
    {
        protected override void SaveValue(string text)
        {
            var value = int.Parse(_inputField.text);
            SetValue(value);
            ShowEditPanel(false);
        }
        
        protected override void SetValue(object value)
        {
            if (value is int convertedValue)
                SetValue(convertedValue);
        }

        private void SetValue(int value)
        {
            base.SetValue(value);
        }
    }
}
