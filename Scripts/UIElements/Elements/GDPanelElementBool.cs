﻿using UnityEngine;
using UnityEngine.UI;

namespace Gameplay.GdPanel.UiGdPanel.Elements
{
    internal class GDPanelElementBool : GDPanelElementField
    {
        [SerializeField] private Toggle _toggle;
        [SerializeField] private Button _editButton;
        
        protected override void SetValue(object value)
        {
            if (value is bool convertedValue)
                SetValue(convertedValue);
        }
        
        private void SetValue(bool value)
        {
            base.SetValue(value);
            _toggle.isOn = value;
        }

        private void OnEnable()
        {
            _toggle.onValueChanged.AddListener(SetValue);
            _editButton.onClick.AddListener(ChangeValue);
        }
        
        private void OnDisable()
        {
            _toggle.onValueChanged.RemoveListener(SetValue);
            _editButton.onClick.RemoveListener(ChangeValue);
        }
        
        private void ChangeValue()
        {
            _toggle.isOn = !_toggle.isOn;
        }
    }
}
