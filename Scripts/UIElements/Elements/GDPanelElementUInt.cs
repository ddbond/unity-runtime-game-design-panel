﻿namespace Gameplay.GdPanel.UiGdPanel.Elements
{
    internal class GDPanelElementUInt : GDPanelElementInputField
    {
        protected override void SaveValue(string text)
        {
            var value = uint.Parse(_inputField.text);
            SetValue(value);
            ShowEditPanel(false);
        }

        protected override void SetValue(object value)
        {
            if (value is uint convertedValue)
                SetValue(convertedValue);
        }

        private void SetValue(uint value)
        {
            base.SetValue(value);
        }
    }
}
