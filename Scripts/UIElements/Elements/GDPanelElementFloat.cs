﻿namespace Gameplay.GdPanel.UiGdPanel.Elements
{
    internal class GDPanelElementFloat : GDPanelElementInputField
    {
        protected override void SaveValue(string text)
        {
            var value = float.Parse(_inputField.text);
            SetValue(value);
            ShowEditPanel(false);
        }

        protected override void SetValue(object value)
        {
            if (value is float convertedValue)
                SetValue(convertedValue);
        }

        private void SetValue(float value)
        {
            base.SetValue(value);
        }
    }
}
