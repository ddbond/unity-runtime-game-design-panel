﻿using TMPro;
using UnityEngine;

namespace Gameplay.GdPanel.UiGdPanel.Elements
{
    internal abstract class GDPanelElement : MonoBehaviour
    {
        [SerializeField] protected TextMeshProUGUI _descriptionTmp;
        
        protected Object _originObj;
        protected string _attributeSource;
        
        internal virtual void Initialization(string description, Object obj, string fieldName)
        {
            _originObj = obj;
            _attributeSource = fieldName;
            SetDescription(description);
        }

        private void SetDescription(string newText)
        {
            _descriptionTmp.text = newText;
        }
    }
}
