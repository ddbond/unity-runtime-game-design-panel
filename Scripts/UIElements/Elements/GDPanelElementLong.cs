﻿namespace Gameplay.GdPanel.UiGdPanel.Elements
{
    internal class GDPanelElementLong : GDPanelElementInputField
    {
        protected override void SaveValue(string text)
        {
            var value = long.Parse(_inputField.text);
            SetValue(value);
            ShowEditPanel(false);
        }

        protected override void SetValue(object value)
        {
            if (value is long convertedValue)
                SetValue(convertedValue);
        }

        private void SetValue(long value)
        {
            base.SetValue(value);
        }
    }
}
