﻿using System;
using Gameplay.GdPanel.Static;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Gameplay.GdPanel.UiGdPanel.Categories
{
    internal class BaseGDPanelCategory : MonoBehaviour
    {
        internal event Action<string> OnSelectCategory;
        private string Path { get; set; }
        [SerializeField] private Button _button;
        [SerializeField] private TextMeshProUGUI _descriptionText;

        internal void Initialization(string description, string path)
        {
            _descriptionText.text = description;
            Path = path;
            GameDesignPanel.GetLevelsPath(Path);
        }

        private void OnEnable()
        {
            _button.onClick.AddListener(ClickButton);
        }

        private void OnDisable()
        {
            _button.onClick.RemoveListener(ClickButton);
        }

        private void ClickButton()
        {
            OnSelectCategory?.Invoke(Path);
        }
    }
}
