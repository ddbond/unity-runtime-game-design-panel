﻿using System;
using Gameplay.GdPanel.UiGdPanel.Categories;
using Gameplay.GdPanel.UiGdPanel.Elements;
using UnityEngine;

namespace Gameplay.GdPanel
{
    [CreateAssetMenu]
    public class GameDesignPanelSettings : ScriptableObject
    {
        [SerializeField] BaseGDPanelCategory category;
        [SerializeField] GDPanelGroup group;
        [SerializeField] GDPanelElementMethod methodPrefab;
        [SerializeField] GDPanelElementFloat floatPrefab;
        [SerializeField] GDPanelElementInt intPrefab;
        [SerializeField] GDPanelElementUInt uintPrefab;
        [SerializeField] GDPanelElementDouble doublePrefab;
        [SerializeField] GDPanelElementLong longPrefab;
        [SerializeField] GDPanelElementBool boolPrefab;

        internal BaseGDPanelCategory GetCategoryPrefab => category;
        internal GDPanelGroup GetGroupPrefab => group;
        internal GDPanelElementMethod GetMethodPrefab => methodPrefab;
        internal GDPanelElementField GetElementPrefab(Type type)
        {
            if (type == typeof(int))
                return intPrefab;
            if (type == typeof(float))
                return floatPrefab;
            if (type == typeof(bool))
                return boolPrefab;
            if (type == typeof(uint))
                return uintPrefab;
            if (type == typeof(double))
                return doublePrefab;
            if (type == typeof(long))
                return longPrefab;

            Debug.LogWarning($"Game Design панель не поддерживает тип поля {type}!");
            return null;
        }
    }
}