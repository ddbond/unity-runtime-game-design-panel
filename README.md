# Unity Runtime GD Panel - Readme

[🇷🇺 Read this in Russian language here](README.ru.md)

<aside>
🖇️ Dependencies: [TextMeshPro](https://docs.unity3d.com/Manual/com.unity.textmeshpro.html), [Odin](https://assetstore.unity.com/packages/tools/utilities/odin-inspector-and-serializer-89041)

</aside>

## ℹ️ Summary

**GD Panel** is a set of tools for the game designer/tester to change various parameters/states during the game.

The GD Panel looks like a separate window with elements with which you can quickly change some setting or somehow affect the game (turn on immortality, reset progress, etc.).

## 💡 Description

With the help of the GD Panel you can:

- Display and call method without parameters
- Display and modify **MonoBehaviour** and **ScriptableObject** fields of the following types: bool, int, float, uint, double, long (including private and static ones)
- Group displayed items by nested categories

When opening the GD Panel automatically searches among all **MonoBehaviour** and **ScriptableObject** *(referenced by the found MonoBehaviour)* components for fields and methods marked with a special attribute, and adds them to itself, allowing you to view their state and edit directly in game.

⚠️ So far, the GD Panel only works with fields and methods that are part of **MonoBehaviour** or **ScriptableObject** objects. It is planned to support properties and class planes.


⚠️ A feature of the GD Panel in the current implementation is that it is updated every time it is opened. This allows you to interact with any entities in real time, but causes a noticeable delay every time you open the GD Panel. It is planned to add manual update / auto-update  only at the start of the scene modes.


## 📋 Usage

To start using, you need to import the package:

[GameDesignPanel.unitypackage](GameDesignPanel.unitypackage)

After the import is completed, you need to add the **Game Design Panel** prefab from the **Prefabs** folder to any scene. GD Panel is a **DontDestroyOnLoad** object, so it is logical to add it to the first scene (ex. Initialization).

- **🏞️ Adding GD Panel**
    
    ![Множественное изменение.gif](Demo_install.gif)
    

When you start the game, a button to open the GD Panel with an icon 🕹️ will be displayed in the upper left corner. If desired, its appearance / location can be changed. The GD Panel can also be opened using the public `ShowPanel()` method on the **Game Design Panel** component.

The system functions with a single attribute. To add any field or method to the GD Panel, just assign an attribute to it:

`[GdPanelExpose]` - Display in the GD Panel and allow the field/method to be modified/called. You can specify subcategories in the attribute parameter, like so: `[GdPanelExpose("Player/Mechanics")]`

The GD Panel is completely reinitialized each time it is opened. It shows the current state of all exposed fields, as well as any exposed methods found so far.

To edit any field, you need to open the GD Panel, find the required field (using the subcategory buttons at the top), click on it, enter the value. To call a method, just click on its button in the GD Panel. Methods in the GD Panel are marked with emoji ⭕.

- **🏞️ Using GD Panel**
    
    ![Демо.gif](Demo.gif)
    

## 👀 Examples

In the package, in the **Examples** folder, there is a prepared scene to demonstrate the capabilities of the panel, as well as a script that shows examples of using the `[GdPanelExpose]` attribute
