﻿using Gameplay.GdPanel.Attributes;
using UnityEngine;

namespace Gameplay.GdPanel.Examples
{
    public class GDPanelTestShow : MonoBehaviour
    {
        [SerializeField] private GDPanelTestScriptableObject scriptableObject;
        
        [GDPanelExpose("Player Settings/Stats")] public int Hp = 100;
        [GDPanelExpose("Player Settings/Stats")] public float Speed = 3.2f;
        
        [GDPanelExpose("Player Settings")] private bool godMode;
        [GDPanelExpose("Player Settings")] private bool extraXP;
        
        [GDPanelExpose] private static bool staticBool;
        
        [GDPanelExpose("Player Settings/Stats")]
        private void ResetStats()
        {
            Debug.Log("ResetStats has been invoked!");
        }

        [GDPanelExpose] public bool publicBooleanField;
        [GDPanelExpose] private bool privateBooleanField = true;
        [GDPanelExpose] private double privateDoubleField = -51f;
        [GDPanelExpose] private float privateFloatField = 44.2f;
        [GDPanelExpose] private uint privateUintField = 25;
        [GDPanelExpose] private int privateIntField = -5;
        [GDPanelExpose] private long privateLongField = 1990412;
        
        [GDPanelExpose("Debug")]
        public void DebugMethod()
        {
            Debug.Log("DebugMethod has been invoked!");
        }
        
        [GDPanelExpose]
        private void TestMethod()
        {
            Debug.Log("TestMethod has been invoked!");
        }
    }
}
