using Gameplay.GdPanel.Attributes;
using UnityEngine;

namespace Gameplay.GdPanel.Examples
{
    [CreateAssetMenu]
    public class GDPanelTestScriptableObject : ScriptableObject
    {
        [GDPanelExpose("ScriptableObject Test")]
        public bool booleanValueInSO = true;
        [GDPanelExpose("ScriptableObject Test")]
        private float floatValueInSO = 2.4f;

        [GDPanelExpose("ScriptableObject Test")]
        private void TestMethod()
        {
            Debug.Log("TestMethod in Scriptable Object has been invoked!");
        }
        
    }
}